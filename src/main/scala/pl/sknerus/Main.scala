package pl.sknerus

import com.typesafe.scalalogging.slf4j.Logging
import pl.sknerus.ItemFilters._

object Main extends App with Logging {
  private val configuration = Configuration()
  main()

  private def main() {
    val searchConfiguration = configuration.searchConfiguration
    val items = new SkapiecCrawler(searchConfiguration.url).getAllItems
    val filtered = filter(items)
    val sorted = filtered.sortBy { item => -item.ratesCount }

    logger.debug(s"before filter: ${items.size}")
    logger.debug(s"after filter: ${filtered.size}")
    saveToFile(sorted)
  }

  private def filter(items: List[SearchItem]): List[SearchItem] = {
    val searchConfiguration = configuration.searchConfiguration

    return items.filter(minimalRatesCountFilter(searchConfiguration.minimalRate))
      .filter(minimalRatesCountFilter(searchConfiguration.minimalRatesCount))
      .filter(onlyWithPrizesFilter)
  }

  private def saveToFile(items: List[SearchItem]) {
    val resultConfiguration = configuration.resultConfiguration
    val htmlSaver = new HtmlSaver(resultConfiguration.file)
    htmlSaver.save(items)
  }
}