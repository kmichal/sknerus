package pl.sknerus

import org.jsoup.nodes.Element

object ItemExtractor {

  def apply(rawItem: Element): SearchItem = {
    val itemExtractor = new ItemExtractor(rawItem)
    val name = itemExtractor.getName
    val url = itemExtractor.getUrl
    val price = itemExtractor.getPrice
    val rate = itemExtractor.getRate
    val ratesCount = itemExtractor.getRatesCount

    SearchItem(name, url, price, rate, ratesCount)
  }
}

class ItemExtractor(item: Element) {

  def getPrice(): Double = {
    val zl = item.select(".price_black .zl").first() match {
      case null => "0"
      case zl => zl.text()
    }
    val gr = item.select(".price_black .gr").first() match {
      case null => "0"
      case gr => gr.text()
    }
    val priceText = s"${zl}${gr}"
    val price = priceText.toDouble
    return price
  }

  def getRatesCount(): Int = {
    val rawRate = item.select(".opinie strong").first()
    val rawRateWithouSpaces = rawRate.text().trim()
    val ratesCount = rawRateWithouSpaces match {
      case "Brak opinii" => 0
      case x => x.toInt
    }
    return ratesCount
  }

  def getRate(): ItemRate = {
    val stars = item.select(".opinia_stars_g").first()
    val starsStyle = stars.attr("style")
    val start = starsStyle.indexOf("width: ") + "width: ".length()
    val end = starsStyle.indexOf("px")
    val starWidth = starsStyle.substring(start, end).toInt
    return ItemRate.fromPixcels(starWidth)
  }

  def getName(): String = {
    val title = item.select(".head a").first()
    val nameWithouSpaces = title.text().trim()
    return nameWithouSpaces
  }

  def getUrl(): String = {
    val title = item.select(".head a").first()
    val url = title.attr("href")
    val fullUrl = s"http://www.skapiec.pl${url}"
    return fullUrl
  }
}