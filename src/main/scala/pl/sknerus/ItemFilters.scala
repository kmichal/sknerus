package pl.sknerus

object ItemFilters {

  def minimalRatesCountFilter(minimalRatesCount: Double): SearchItem => Boolean = {
    item => item.ratesCount >= minimalRatesCount
  }

  def minimalRateFilter(minimalRate: ItemRate): SearchItem => Boolean = {
    item => item.rate.rate >= minimalRate.rate
  }

  def onlyWithPrizesFilter: SearchItem => Boolean = {
    item => item.price > 0
  }
}
