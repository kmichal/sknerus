package pl.sknerus

import scala.language.implicitConversions

case class ItemRate(rate: Double)

object ItemRate {
  def fromPixcels(starsWidthInPixels: Int): ItemRate = {
    val normalizedRate = starsWidthInPixels * 5.0 / 75.0
    return ItemRate(normalizedRate)
  }

  implicit def fromRawDouble(rate: Double): ItemRate = ItemRate(rate)
}