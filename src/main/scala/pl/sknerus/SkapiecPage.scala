package pl.sknerus

import scala.collection.JavaConversions._
import org.jsoup.Jsoup
import org.jsoup.nodes.Element

class SkapiecPage(pageUrl: String) {

  private def getRawItems(): List[Element] = {
    val page = Jsoup.connect(pageUrl).timeout(5000).get()
    val items = page.select(".p_rect").toList
    return items
  }

  def getAllItems(): List[SearchItem] = {
    val rawItems = getRawItems()
    val items = rawItems.map { rawItem => ItemExtractor(rawItem) }
    return items
  }
}