package pl.sknerus

case class SearchItem(name: String, url: String, price: Double, rate: ItemRate, ratesCount: Int)