package pl.sknerus

import collection.JavaConversions._
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import com.typesafe.scalalogging.slf4j.Logging

class SkapiecCrawler(pageUrl: String) extends Logging {

  val page = Jsoup.connect(pageUrl).timeout(5000).get()

  def getAllItems(): List[SearchItem] = {
    val lastPage = getLastPageNumber
    val allItems = getItemsFromPageRange(lastPage)
    return allItems
  }

  def getItemsFromPageRange(lastPage: Int): List[SearchItem] = {
    val pages = getPageRange(lastPage)
    val items = getItemsFromPages(pages)
    return items
  }

  private def getItemsFromPages(pages: List[SkapiecPage]): List[SearchItem] = {
    val items = pages.foldLeft(List[SearchItem]())((acc, page) => acc ::: page.getAllItems)
    return items
  }

  private def getPageRange(lastPage: Int): List[SkapiecPage] = {
    val pages = (1 to lastPage).toList.map { pageNumber =>
      val url = s"${pageUrl}/page/${pageNumber}"
      logger.debug(s"url: $url")
      new SkapiecPage(url)
    }

    return pages
  }

  private def getLastPageNumber(): Int = {
    def isNumeric(input: String): Boolean = input.forall(char => char.isDigit)

    def lastPage(reversePageList: List[Element]): Int = reversePageList match {
      case head +: _ if isNumeric(head.text()) => head.text().toInt
      case _ +: tail => lastPage(tail)
      case _ => 1
    }

    val pageCount = page.select("#pagesbox > a").toList
    lastPage(pageCount.reverse)
  }
}