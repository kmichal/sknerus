package pl.sknerus

object SaveToFile {

  def printToFile(file: java.io.File)(block: java.io.PrintWriter => Unit) {
    val writer = new java.io.PrintWriter(file, "utf-8")
    try {
      block(writer)
    } finally {
      writer.close()
    }
  }
}

class HtmlSaver(fileName: String) {

  private val start = "<!doctype html><html><head><meta charset=\"utf-8\"></head><body>"

  private val end = "</body></html>"

  private def toHtml(item: SearchItem): String = {
    val html = <div>
                 <a href={ item.url }>{ item.name }</a><br/>
                 Cena: { item.price } zł<br/>
                 Ocena: { item.rate.rate } ({ item.ratesCount })<br/>
               </div>
    return html.toString
  }

  def save(items: List[SearchItem]) {
    val file = new java.io.File(fileName)
    SaveToFile.printToFile(file) { writer =>
      writer.println(start)
      items.foreach { item =>
        val itemInHtml = toHtml(item)
        writer.println(itemInHtml)
      }
      writer.println(end)
    }
  }
}