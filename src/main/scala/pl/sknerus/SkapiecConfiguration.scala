package pl.sknerus

import java.io.{FileInputStream, InputStream, File}
import net.liftweb.json._
import scala.io.Source

case class SearchConfiguration(url: String, minimalRate: Double, minimalRatesCount: Int)

case class ResultConfiguration(file: String)

case class Configuration(searchConfiguration: SearchConfiguration, resultConfiguration: ResultConfiguration)

object Configuration {

  implicit val formats = DefaultFormats

  def apply(): Configuration = {
    val skapiecJSON = getRawJsonFromResource("sknerus.json")
    return Configuration(skapiecJSON)
  }

  private def getRawJsonFromResource(resourcePath: String): String = {
    val jsonStream = getJSONStream(resourcePath)
    val rawJson = Source.fromInputStream(jsonStream, "utf-8").mkString
    return rawJson
  }

  def apply(rawJson: String): Configuration = {
    val json = parse(rawJson)
    return json.extract[Configuration]
  }

  private def getJSONStream(configFileName: String): InputStream = {
    val configFile = new File(configFileName)
    if (configFile.exists()) {
      new FileInputStream(configFile)
    } else {
      getClass().getResourceAsStream("/" + configFileName)
    }
  }
}